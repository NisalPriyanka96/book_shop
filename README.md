## Instructions to install

1. Install XAMPP

2. Copy project folder to xampp >> htdocs

3. Go to http://localhost/phpmyadmin

4. Create database.

5. Database name should be "book"

6. Execute following sql queries into "book" database

### SQL QUERIES
```
create table books
(
	isbn varchar(70),
	name varchar(2000),
	author varchar(2000),

	constraint pk_book PRIMARY KEY(isbn)
);


CREATE TABLE customer
(
  bill_no varchar(60),
  purchased_qty varchar(500),
  ISBN varchar(60),
    
  constraint pk_customer PRIMARY KEY(bill_no),
  CONSTRAINT fk_customer FOREIGN KEY(ISBN) REFERENCES books(isbn)
  
);


CREATE TABLE book_quentyty
(
    isbn varchar(70),
    total_books varchar(80),
    
    CONSTRAINT pk_sales PRIMARY KEY(isbn),
    CONSTRAINT fk_sales FOREIGN KEY(isbn) REFERENCES books(isbn)
    
 );

create table book_price
(
    isbn varchar(70),
    selling_price varchar(60),
    cost varchar(60),
    
    CONSTRAINT pk_bookprice PRIMARY KEY(isbn),
    CONSTRAINT fk_bookprice FOREIGN KEY(isbn) REFERENCES books(isbn)
    
    );

create table publisher
(
    isbn varchar(70),
    publisher_name varchar(200),
    address varchar(200),
    telephone varchar(60),
    
    CONSTRAINT pk_publisher PRIMARY KEY(isbn),
    CONSTRAINT fk_publisher FOREIGN KEY(isbn) REFERENCES books(isbn)
    
    );

```