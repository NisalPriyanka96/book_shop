<?php
    session_start();
    include 'func/db_connect.php';

    //$_SESSION['login'] = "false";

    if(isset($_GET['add_to_cart']))
    {
       if(isset($_SESSION["shopping_cart"]))
       {
            $item_array_id = array_column($_SESSION["shopping_cart"], "item_id");

            if(!in_array($_GET["id"], $item_array_id))
            {
                $count = count($_SESSION["shopping_cart"]);

                $item_array = array(
                    'item_id' => $_GET["id"],
                    'item_name' => $_GET["hidden_name"],
                    'item_price' => $_GET["hidden_price"],
                    'item_quantity' => $_GET["quantity"],
                );
                $_SESSION["shopping_cart"][$count] = $item_array;
            }
            else
            {
                echo '<script> alert("Item Already Added") </script>';
                echo '<script> window.location="index.php" </script>';
            }

       }
       else
       {
           // Add items to item_array (no values inside cart) --> to display inside cart
            $item_array = array(
                'item_id' => $_GET["id"],
                'item_name' => $_GET["hidden_name"],
                'item_price' => $_GET["hidden_price"],
                'item_quantity' => $_GET["quantity"],
            );
            $_SESSION["shopping_cart"][0] = $item_array;
       }
    }

    // remove item from cart
    if(isset($_GET["action"]))
    {
        if($_GET["action"]=="delete")
        {
            foreach($_SESSION["shopping_cart"] as $keys => $values)
            {
                if($values["item_id"]==$_GET["id"])
                {
                    unset($_SESSION["shopping_cart"][$keys]);
                    echo '<script>alert("Item Removed")</script>';  
                    echo '<script>window.location="index.php"</script>';

                }
            }
        }

         // checkout items
        if($_GET["action"]=="checkout")
        {
            if(isset($_SESSION['login']))
            {
                echo "<script> alert('payment sucess!!') </script>";
                echo '<script>window.location="index.php"</script>';
            }
            else
            {
                echo "<script>alert('Login to continue..')</script>";
                echo '<script>window.location="index.php"</script>';
            }
        }
    }

    

   
?>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Book Shop</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container-fluid">

    <div class="row justify-content-center" style="background-color:orange"> 
    
    <div class="col-md-10 text-center"> <h1> BOOK STORE  </h1> </div>
    <div class="col-md-2 text-right"> 
    <div style="margin-top:5px;"></div>
    <input type="button" value="Login" class="btn btn-info" data-toggle="modal" data-target="#login" id="btnlgn"/> 
    <a href="index.php?actions=logout"><div class="text-primary" id="lgnName"></div></a>
    </div>
    
    </div>
    
    <!-- LOGIN CHECK -->
    <?php
        if(isset($_POST['loginbtn']))
        {
            if(empty($_POST["emailtxt"]) && empty($_POST["passwordtxt"]))
            {
                echo "<script> alert('Username or Password empty!!'); </script>";
            }
            else
            {
                $_SESSION['login'] = true;
                $name = $_POST['emailtxt'];
    ?>
                <script>
                    var lgn = document.getElementById("btnlgn");
                    var lgnName = document.getElementById("lgnName");

                    lgn.style.display = "none";
                    lgnName.innerHTML = "LogOut";

                </script>

    <?php
            }
        }
        if(isset($_SESSION['login']) && $_SESSION['login'] == "true")
        {
    ?>
                <script>
                    var lgn = document.getElementById("btnlgn");
                    var lgnName = document.getElementById("lgnName");

                    lgn.style.display = "none";
                    lgnName.innerHTML = "LogOut";
                    //window.location="index.php";

                </script> 
    <?php
        }
        // if user click log out button
        if(isset($_GET["actions"]) == 'logout' )
            {
                $_SESSION['login'] == "false";
                unset($_SESSION['login']);
    ?>
                <script>
                    var lgn = document.getElementById("btnlgn");
                    var lgnName = document.getElementById("lgnName");

                    lgn.style.display = "block";
                    lgnName.style.display = "none";

                    window.location="index.php";

                </script> 
    <?php
            }
    ?>

    <div class = "row"> <div style="padding:10px;"></div>  </div>

    <!-- DISPLAY BOOKS IN DATABASE -->
    <div class="row">
        <?php
            $sql = "select books.isbn, books.name, books.author, book_price.selling_price, book_quentyty.total_books
            from books, book_price, book_quentyty
            WHERE books.isbn = book_price.isbn AND books.isbn = book_quentyty.isbn";
            $result = mysqli_query($conn, $sql);
            if(mysqli_num_rows($result)>0)
            {
                while($row = mysqli_fetch_array($result))
                {
            
        ?>
              <div class="col-md-2">
                    <div class="card">
                        <form method="GET" action="index.php?action=add&id=<?php echo $row["isbn"]; ?>">
                        <div class="card-header">
                            <?php echo $row['name']; ?>
                        </div>
                        <div class="card-body">
                        <img src="func/book.jpeg" width="200" height="200" class="img-responsive" /> <br/>
                          <div style="padding-top:10px;">  Price : LKR <?php echo $row['selling_price']; ?>.00 </div>
                          <div class="row">
                            <div class="col-md-9">
                                <input type="text" name="quantity" class="form-control form-control-sm" value="1"/>
                            </div>
                            <div class="col-md-3">
                               <span class="text-danger">/ <?php echo $row['total_books']; ?></span>
                            </div>
                            </div>
                            <input type="hidden" name="hidden_name" value="<?php echo $row['name']; ?>" />
                            <input type="hidden" name="hidden_price" value="<?php echo $row['selling_price']; ?>" />
                            <input type="hidden" name="id" value="<?php echo $row['isbn']; ?>" />
                            <input type="submit" name="add_to_cart" style="margin-top:5px;" class="btn btn-success" value="Add to Cart" />
                        </form>
                        </div>
                        
                        </div>           
                </div>

        <?php 
                }
            }  
        ?>
    </div>
    <!-- SHOPPING CART -->
    <div class = "row"> <div style="padding:10px;"></div>  </div>
    <div class="row">
        <div class="col-md-12"> <h4 style="margin-left:12px;"> Order Details </h4> </div>
    </div>
    <div class="row" style="margin-left:12px; padding-top:5px;">
        <div class = "col-md-6">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <tr>
                        <th width="40%">Item Name</th>  
                        <th width="10%">Quantity</th>  
                        <th width="20%">Price</th>  
                        <th width="15%">Total</th>  
                        <th width="5%">Action</th>
                    </tr>
                    <?php
                        if(!empty($_SESSION["shopping_cart"])) 
                        {
                            $total = 0;
                            // read all the elements inside shopping cart session variable.
                            foreach($_SESSION["shopping_cart"] as $keys => $values)
                            {
                    ?>
                                <tr>
                                    <td> <?php echo $values["item_name"]; ?> </td>
                                    <td> <?php echo $values["item_quantity"]; ?> </td>
                                    <td>LKR <?php echo $values["item_price"]; ?>.00 </td>
                                    <td>LKR <?php echo number_format($values['item_quantity'] * $values['item_price'], 2) ?> </td>
                                    <td> <a href="index.php?action=delete&id=<?php echo $values['item_id']; ?>"> <span class="text-danger">Remove</span></a></td> </a></td>
                                </tr>
                    <?php    
                                $total = $total + ($values["item_quantity"]* $values["item_price"]);           
                            }
                    ?>
                                <tr>
                                    <td colspan="3" align="right">Total</td>
                                    <td align="right">LKR <?php echo number_format($total, 2); ?></td>  
                                    <td></td>
                                </tr>

                                <tr>
                                    <td colspan="3" align="right"></td>
                                    <td align="right"> <button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#checkout">CHECKOUT</button> </td>
                                </tr>
                    <?php

                        }
                    ?>
                </table>
            </div>
        </div>
    </div>

    <!-- modal -->
    <div class="modal fade" id="checkout" tabindex="-1" role="dialog" aria-labelledby="Checkout" aria-hidden="true">
         <div class="modal-dialog" role="document"> 
            <div class="modal-content">
               <div class="modal-header">
               <h5 class="modal-title" id="checkouthead">CHECKOUT</h5>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
               </div>
            <div class="modal-body">
                <span class="text-danger" style="font-size:24px"> Total Price : <?php echo number_format($total, 2);  ?> </span> <br/>
                <div style="padding:10px;"> </div>
                <a href="index.php?action=checkout&price=<?php echo $total; ?>" > <span class="btn btn-info"> Pay </span> </a>
            </div>
            </div>
         </div>           
    </div>

    <!-- Login modal -->
    <div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="loginm" aria-hidden="true">
         <div class="modal-dialog" role="document"> 
            <div class="modal-content">
               <div class="modal-header">
               <h5 class="modal-title" id="loginhead">Login</h5>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
               </div>
            <div class="modal-body">
                <form method="POST" action="<?php echo $_SERVER['PHP_SELF'];?>">
                        <div class="form-group">
                            <label for="email">Email address</label>
                            <input type="email" class="form-control" name="emailtxt" placeholder="Enter email" />
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" name="passwordtxt" placeholder="Enter password" />
                        </div>
                            <input type="submit" class="btn btn-primary" value="LogIn" name="loginbtn" />
                        
                </form>
            </div>
            </div>
         </div>           
    </div>

</div>

</body>
</html>